﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private float jumpForce = 10f;

    public float JumpForce { get => jumpForce; set => jumpForce = value; }

    public Rigidbody2D Rigidbody2D;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump") || Input.GetMouseButtonDown(0))
        {
            Rigidbody2D.velocity = Vector2.up * JumpForce;
        }
    }
}
